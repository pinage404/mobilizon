# Mobilizon

## Development

### Docker

Just run
```
docker-compose up -d
```
to start a database container, an API container and the front-end dev container running on localhost:80

### Manual

#### Server

  * Install Elixir (and Erlang) by following the instructions at [https://elixir-lang.github.io/install.html](https://elixir-lang.github.io/install.html)
  * Install PostgreSQL
  * Start postgres and make sure the postgis extension is installed.
  * Create a postgres user with database creation capabilities, using the
    following: `createuser -d -P elixir` and set `elixir` as the password.
  * Fetch dependencies with `mix deps.get`.
  * Create your database with `mix ecto.create`.
  * Create the postgis extension on the database with a postgres user that has
    superuser capabilities: `psql mobilizon_dev`

    ``` create extension if not exists postgis; ```

  * Run migrations: `mix ecto.migrate`.
  * Start Phoenix endpoint with `mix phx.server`.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser
and see the website (server *and* client) in action.

#### Client

If you plan to specifically change the client side (frontend), do the following
once the server is running:

  * Install the latest version of NodeJS
  * Change directory to `js/`.
  * Install JavaScript package dependencies: `npm install`.
  * Run the developement server in watch mode: `npm run dev`. This will open a
    browser on [`localhost:8080`](http://localhost:8080) that gets
    automatically reloaded on change.

## Production

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
